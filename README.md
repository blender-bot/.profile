# Blender Bot for Gitea

Automation for building pull requests and triaging issues.

See [Blender Bot documentation](https://projects.blender.org/infrastructure/blender-bot/src/branch/main/README.md) for available commands.